const express = require('express')
const cors = require('cors');
require('./config/db');
const path = require('path');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

const routes = require('./routes/index');
routes(app);

app.use('/', express.static('./client/build'));

const port = process.env.PORT || 3050;

app.get('*', (req, res) => {
	res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
});

app.listen(port, (error) => {
	if (error) {
		return console.log(`Error ${error}`);
	}
	console.log(`Server started to address http://localhost:${port}`);
});

exports.app = app;