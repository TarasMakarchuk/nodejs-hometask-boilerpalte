const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)

        const { email, password } = req.body;
        const userData = AuthService.login({ email, password});

        if (userData && userData.email === email && userData.password === password) {
            res.data = userData;
            res.status(200).json("Authorized");
        } else if (!userData) {
            res.status(401).json({
                error: true,
                message: "Unauthorized"
            });
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;