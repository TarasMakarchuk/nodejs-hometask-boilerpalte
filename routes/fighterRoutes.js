const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', function (req, res, next) {
	const result = FighterService.getAllFightersData();
	if (result) {
		res.status(200).send(FighterService.getAllFightersData());
	} else {
		res.status(404).json({
			error: true,
			message: `Data is not found`,
		});
	}
});

router.get('/:id', function (req, res, next) {
	const { id } = req.params;
	const result = FighterService.getFighterById(id);
	if (result) {
		res.status(200).json(result);
	} else {
		res.status(404).json({
			error: true,
			message: `User id is not found`,
		});
	}
});

router.post('/', createFighterValid, function (req, res, next) {
	if (createFighterValid(req, res, next)) {
		const result = FighterService.saveFighterData(req.body);
		if (result) {
			res.status(200).json(result);
		} else {
			res.status(400).json({
				error: true,
				message: `Fighter is not created`,
			});
		}
	}
});

router.put('/:id', updateFighterValid, function (req, res, next) {
	const { id } = req.params;
	if (createFighterValid(req, res, next)) {
		const fighterDbId = FighterService.getFighterById(id);
		if (!fighterDbId) {
			res.status(404).json({
				error: true,
				message: `Fighter id is not found`,
			});
		}
		const result = FighterService.updateFighterData(id, req.body);
		if (result) {
			res.status(200).json(result);
		} else {
			res.status(404).json({
				error: true,
				message: `Fighter is not found`,
			});
		}
	}
});


router.delete('/:id', function (req, res, next) {
	const { id } = req.params;
	const fighterDbId = FighterService.getFighterById(id);
	if (!fighterDbId) {
		res.status(404).json({
			error: true,
			message: `Fighter id is not found`,
		});
	}
	const result = FighterService.deleteFighter(id);
	if (result) {
		res.status(200).json(result);
	} else {
		res.status(404).json({
			error: true,
			message: `Fighter id is not found`,
		});
	}
});

module.exports = router;