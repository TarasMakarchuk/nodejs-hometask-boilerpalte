const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', function (req, res, next) {
	const result = UserService.getAllUsersData();
	if (result) {
		res.status(200).send(UserService.getAllUsersData());
	} else {
		res.status(404).json({
			error: true,
			message: `Data is not found`,
		});
	}
});

router.get('/:id', function (req, res, next) {
	const { id } = req.params;
	const result = UserService.getUserById(id);
	if (result) {
		res.status(200).json(result);
	} else {
		res.status(404).json({
			error: true,
			message: `User id is not found`,
		});
	}
});

router.post('/', createUserValid, function (req, res, next) {
	if (createUserValid(req, res, next)) {
		const result = UserService.saveUserData(req.body);
		if (result) {
			res.status(200).json(result);
		} else {
			res.status(400).json({
				error: true,
				message: `User is not created`,
			});
		}
	}
});

router.put('/:id', updateUserValid, function (req, res, next) {
	const { id } = req.params;
	const userDbId = UserService.getUserById(id);
	if (!userDbId) {
		res.status(404).json({
			error: true,
			message: `User id is not found`,
		});
	}
	if (updateUserValid(req, res, next)) {
		const result = UserService.updateUserData(id, req.body);
		if (result) {
			res.status(200).json(result);
		} else {
			res.status(404).json({
				error: true,
				message: `User is not found`,
			});
		}
	}
});

router.delete('/:id', function (req, res, next) {
	const { id } = req.params;
	const userDbId = UserService.getUserById(id);
	if (!userDbId) {
		res.status(404).json({
			error: true,
			message: `User id is not found`,
		});
	}
	const result = UserService.deleteUser(id);
	if (result) {
		res.status(200).json(result);
	} else {
		res.status(404).json({
			error: true,
			message: `User id is not found`,
		});
	}
});

module.exports = router;