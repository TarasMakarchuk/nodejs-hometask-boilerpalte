const { createProxyMiddleware } = require('http-proxy-middleware');
const port = process.env.PORT || 3050;
module.exports = function (app) {
	app.use(createProxyMiddleware('/api/',
		{target: `http://localhost:${port}`}
	));
}
