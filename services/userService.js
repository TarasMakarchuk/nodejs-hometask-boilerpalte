const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    getAllUsersData = () => UserRepository.getAll();

    getUserById = id => {
        if (id) {
            return UserRepository.getOneById(id);
        }
        return null;
    };

    saveUserData = data => {
        if (data) {
            return UserRepository.create(data);
        }
        return null;
    };

    updateUserData = (id, data) => {
        if (data) {
            return UserRepository.update(id, data);
        }
        return null;
    };

    deleteUser = id => {
        if (id) {
            return UserRepository.delete(id);
        }
        return null;
    };

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();