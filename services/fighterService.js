const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

	getAllFightersData = () => FighterRepository.getAll();

	getFighterById = id => {
		if (id) {
			return FighterRepository.getOneById(id);
		}
		return null;
	};

	saveFighterData = data => {
		if (data) {
			return FighterRepository.create(data);
		}
		return null;
	};

	updateFighterData = (id, data) => {
		if (id && data) {
			return FighterRepository.update(id, data);
		}
		return null;
	};

	deleteFighter = id => {
		if (id) {
			return FighterRepository.delete(id);
		}
		return null;
	};

	search(search) {
		const item = FighterRepository.getOne(search);
		if (!item) {
			return null;
		}
		return item;
	}

	searchIgnoreCase(search) {
		const item = FighterRepository.getOneIgnoreCase(search);
		if (!item) {
			return null;
		}
		return item;
	}
}

module.exports = new FighterService();