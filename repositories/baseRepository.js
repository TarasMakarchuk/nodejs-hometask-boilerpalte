const { dbAdapter } = require('../config/db');
const { v4 } = require('uuid');


class BaseRepository {
    constructor(collectionName) {
        this.dbContext = dbAdapter.get(collectionName);
        this.collectionName = collectionName;
    }

    generateId() {
        return v4();
    }

    getAll() {
        return this.dbContext.value();
    }

    getOneById(id) {
        return this.dbContext.find({ id }).value();
    }

    getOne(search) {
        return this.dbContext.find(search).value();
    }

    getOneIgnoreCase(search) {
        const dbContextCopy = this.dbContext;
        const searchCopy = search;
        const key = Object.keys(searchCopy)[0];
        const value = Object.values(searchCopy)[0];
        return dbContextCopy.find(item => item[key].toUpperCase().trim() === value.toUpperCase().trim()).value();
    }

    create(data) {
        data.id = this.generateId();
        data.createdAt = new Date();
        const list = this.dbContext.push(data).write();
        return list.find(it => it.id === data.id);
    }

    update(id, dataToUpdate) {
        dataToUpdate.updatedAt = new Date();
        return this.dbContext.find({ id }).assign(dataToUpdate).write();
    }

    delete(id) {
        return this.dbContext.remove({ id }).write();
    }
}

exports.BaseRepository = BaseRepository;