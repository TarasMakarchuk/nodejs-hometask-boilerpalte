const { user } = require('../models/user');
const { validateData, validateFields, validateUsersDuplicates } = require('../middlewares/validation.middleware');

const emailPattern = new RegExp(/([a-zA-Z0-9]+)([.{1}])?([a-zA-Z0-9]+)@gmail([.])com/g);
const phoneNumberPattern = new RegExp(/^\+380\d{9}$/);
const MIN_FIRST_NAME_LENGTH = 2;
const MIN_LAST_NAME_LENGTH = 2;
const MIN_PASSWORD_LENGTH = 3;

const validateCreateUser = ((req, res, next) => {
const { firstName, lastName, email, phoneNumber, password } = req.body;

    validateData(req, res);
    validateFields(req, res, user);
    validateUsersDuplicates(req, res, {"email": `${email}`});
    validateUsersDuplicates(req, res, {"phoneNumber": `${phoneNumber}`});

    if (!firstName) {
        res.status(400).send({
            error: true,
            message: `First name is required`,
        });
        return false;
    } else if (firstName.length < MIN_FIRST_NAME_LENGTH) {
        res.status(400).send({
            error: true,
            message: `FirstName should be less than ${MIN_FIRST_NAME_LENGTH} symbols`,
        });
        return false;
    }

    if (!lastName) {
        res.status(400).send({
            error: true,
            message: `Last name is required`,
        });
        return false;
    } else if (lastName.length < MIN_LAST_NAME_LENGTH) {
        res.status(400).send({
            error: true,
            message: `LastName should be less than ${MIN_LAST_NAME_LENGTH} symbols`,
        });
        return false;
    }

    if (!email) {
        res.status(400).send({
            error: true,
            message: `Email is required`,
        });
        return false;
    } else if (!email.match(emailPattern)) {
        res.status(400).send({
            error: true,
            message: `Email ${email} should be registered on gmail.com`,
        });
        return false;
    }

    if (!phoneNumber) {
        res.status(400).send({
            error: true,
            message: `Phone number is required`,
        });
        return false;
    } else if (!phoneNumber.match(phoneNumberPattern)) {
        res.status(400).send({
            error: true,
            message: `Phone number ${phoneNumber} should be in the format +380xxxxxxxxx`,
        });
        return false;
    }

    if (!password) {
        res.status(400).send({
            error: true,
            message: `Password is required`,
        });
        return false;
    } else if (password.length < MIN_PASSWORD_LENGTH) {
        res.status(400).send({
            error: true,
            message: `Password length should be more that ${MIN_PASSWORD_LENGTH} symbols`,
        });
        return false;
    }
    return true;
});

const createUserValid = (req, res, next) => {
    // TODO: Implement validator for user entity during creation

    const valid = validateCreateUser(req, res, next);
    next();
    return valid;
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validator for user entity during update

    const valid = validateCreateUser(req, res, next);
    next();
    return valid;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;