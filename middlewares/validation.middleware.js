const UserService = require('../services/userService');
const FighterService = require('../services/fighterService');

const validateData = (req, res) => {
	if (!req && !req.body) {
		res.status(400).send({
			error: true,
			message: `Data is not received`,
		});
		return false;
	}
};

const validateFields = (req, res, role) => {
	const requestFields = Object.keys(req.body);
	const userFields = Object.keys(role).filter(item => item !== 'id');
	const checkFields = (array, target) => target.every(field => array.includes(field));
	const areFieldsValid = checkFields(userFields, requestFields);

	if (!areFieldsValid) {
		res.status(400).send({
			error: true,
			message: `Fields names are incorrect`,
		});
		return false;
	}
};

const validateUsersDuplicates = (req, res, search) => {
	const duplicateEmail = UserService.search(search);
	if (duplicateEmail === null) {
		return true;
	} else {
		res.status(400).send({
			error: true,
			message: `Field already exists`,
		});
		return false;
	}
}

const validateFightersNamesDuplicates = (req, res) => {
	const { name } = req.body;
	const searchDb = FighterService.searchIgnoreCase({"name": `${name}`});
	if (searchDb === null && name) {
		return true;
	} else {
		res.status(400).send({
			error: true,
			message: `Fighter name already exists`,
		});
		return false;
	}
}

module.exports = {
	validateData,
	validateFields,
	validateUsersDuplicates,
	validateFightersNamesDuplicates,
};
