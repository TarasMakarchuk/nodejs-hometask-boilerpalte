const { fighter } = require('../models/fighter');
const { validateData, validateFields, validateFightersNamesDuplicates } = require('../middlewares/validation.middleware');

const MIN_POWER = 1;
const MAX_POWER = 100;
const MIN_DEFENSE = 1;
const MAX_DEFENSE = 10;
const MIN_HEALTH = 80;
const MAX_HEALTH = 120;
const MIN_NAME_LENGTH = 1;

const validateFighter = (req, res, next) => {
    req.body.health === undefined ? req.body.health = 100 : req.body.health;
    const { name, power, defense, health } = req.body;

    validateData(req, res);
    validateFields(req, res, fighter);

    if (!name) {
        res.status(400).send({
            error: true,
            message: `Name is required`,
        });
        return false;
    } else if (name.length < MIN_NAME_LENGTH) {
        res.status(400).send({
            error: true,
            message: `Name should be less than ${MIN_NAME_LENGTH} symbols`,
        });
        return false;
    }

    validateFightersNamesDuplicates(req, res);

    if (!power) {
        res.status(400).send({
            error: true,
            message: `Power is required`,
        });
        return false;
    } else if (!Number.isInteger(power)) {
        res.status(400).send({
            error: true,
            message: `Power should be number`,
        });
        return false;
    } else if (power < MIN_POWER || power > MAX_POWER) {
        res.status(400).send({
            error: true,
            message: `Power should be between ${MIN_POWER} and ${MAX_POWER}`,
        });
        return false;
    }

    if (!defense) {
        res.status(400).send({
            error: true,
            message: `Defense is required`,
        });
        return false;
    } else if (!Number.isInteger(defense)) {
        res.status(400).send({
            error: true,
            message: `Defense should be number`,
        });
        return false;
    } else if (defense < MIN_DEFENSE || defense > MAX_DEFENSE) {
        res.status(400).send({
            error: true,
            message: `Defense should be between ${MIN_DEFENSE} and ${MAX_DEFENSE}`,
        });
        return false;
    }

    if (!Number.isInteger(health)) {
        res.status(400).send({
            error: true,
            message: `Health should be number`,
        });
        return false;
    } else if (health < MIN_HEALTH || health > MAX_HEALTH) {
        res.status(400).send({
            error: true,
            message: `Health should be between ${MIN_HEALTH} and ${MAX_HEALTH}`,
        });
        return false;
    }
    return true;
};

const createFighterValid = (req, res, next) => {
    // TODO: Implement validator for fighter entity during creation

    const valid = validateFighter(req, res, next);
    next();
    return valid;
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validator for fighter entity during update

    const valid = validateFighter(req, res, next);
    next();
    return valid;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;